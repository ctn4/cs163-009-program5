#include "graph.h"

/*
   This graph.cpp file implements the member functions in
   the graph.h file. Functions include a constructor, destructor,
   insert vertex, add edge, display edges and display all.
 */


// constructor
table::table(int size)
{
	list_size = size;	
	adjacency_list = new vertex[size];
	for(int i {0}; i < size; ++ i)
	{
		adjacency_list[i].plan = "";
		adjacency_list[i].head = nullptr;
	}
}


// destructor
table::~table()
{
	for (int i {0}; i < list_size; ++i)
	{
		edge * current = adjacency_list[i].head;
		while(current)
		{
			edge * hold = current->next;
			delete current;
			current = hold;
		}
	}
	delete [] adjacency_list;
	adjacency_list = nullptr;
}


// This function inserts a vertex
int table::insertVertex(const string & to_add)
{
	for(int i {0}; i < list_size; ++i)
	{	
		if(adjacency_list[i].plan == "")
		{
			adjacency_list[i].plan = to_add;
			return 1;	
		}
	}
	return 0;
}


// This function connects 2 vertices together
int table::insertEdge(string currentVertex, string attach)
{
	int currentIndex = -1;
	int attachIndex = -1;
	for (int i {0}; i < list_size; ++i)
	{
		if (adjacency_list[i].plan == currentVertex)
			currentIndex = i;
		if (adjacency_list[i].plan == attach)
			attachIndex = i;
	}
	if (currentIndex == -1 || attachIndex == -1)
		return 0;
	edge * newEdge = new edge;
	newEdge->adjacent = &adjacency_list[attachIndex];
	newEdge->next = adjacency_list[currentIndex].head;
	adjacency_list[currentIndex].head = newEdge;
	return 1;

}


// This function displays all the connections between vertices
int table::displayAdjacent(const string & key)
{
	int index = 0;
	while (index < list_size && adjacency_list[index].plan != key)
		++index;
	if (index >= list_size)
		return 0;
	cout << "Connected to: " << endl;
	if(!adjacency_list[index].head)
		cout << "NO CONNECTIONS" << endl;
	else
		displayAdjacentR(adjacency_list[index].head);
	return 1;
}


// This is the recursive funtion for display adjacents 
int table::displayAdjacentR(edge * current)
{
	if (current)
	{
		cout << current->adjacent->plan << " ";
		displayAdjacentR(current->next);
	}
	return 0;
}


// This function displays all the vertices
int table::displayAll()
{
	int count = 0;
	displayAll(0, count);
	return count; // + displayAll(0, count);
}


// The recursive function for displaying all the vertices and edges
int table::displayAll(int index, int & count)
{
	if (index >= list_size)
		return 1;
	if (adjacency_list[index].plan.length() != 0)
	{
		cout << "\nVertex #" << index + 1 << ": " << endl << adjacency_list[index].plan << endl;
		++count;
		displayAdjacent(adjacency_list[index].plan);
		cout << endl << endl;
	}
	return displayAll(index + 1, count);	
}


