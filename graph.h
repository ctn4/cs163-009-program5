#include <iostream>
#include <string>
#include <cctype>
using namespace std;

/*
   Christine Nguyen, CS163, Progrram 5, 06/13/23
   The purpose of the this grapgh.h file is to create
   the vertex struct, edge struct, and table class to
   build a directed graph. The adjacency list of this program
   wil be an array of vertex objects, which will have head pointers
   for each linear linked list which will represent as the edge list.
 */

// vertex for plan
struct vertex	
{
	string plan;	// unsure if wanting an object of plans or just one string??
	struct edge * head;	// edge list plan
};


// the edge list
struct edge
{
	vertex * adjacent;	// adjacent vertices
	edge * next;	// pointer to the next vertex
};


// the table class that will build the adjacency list
class table
{
	public:
		table(int size);	// constructor
		~table();	// destructor
		int insertVertex(const string & to_add);	// insert a plan
		int insertEdge(string currentVertex, string attach);	// insert connection	
		int displayAdjacent(const string & key);	// displays connecting vertices
		int displayAll();	// displays all, calls the displayAll recursive and displayAdjacent functions 
	private:
		vertex * adjacency_list;	// pointer to list of vertices
		int list_size;		// size of the list
		int displayAdjacentR(edge * current);	// the recursive function for displayAdjacent
		int displayAll(int index, int & count);	// the recursive function for displayAll
		
};


