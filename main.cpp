#include "graph.h"

/*
	This main.cpp files uses a menu to test out all the functions
	of the graph.h file. From then menu you are able to call/execute
	any function in different orders, also well as error checks to see
	if the function was able to be successful or not. The size is fixed.
*/

int main()
{
	int size {10};
	table graph(size);
	int choice {0};
	string myPlan = "";
	string attachPlan = "";
	int result {0};

	cout << "\n\nWelcome to the Planning Program!" << endl << endl
		<< "This program is designed for you to add up to 10 plans/todo items" << endl
		<< "and give you the ability to connect them up to other plans/todo items." << endl;

	do
	{
		cout << "\n*********MENU*********" << endl
			<< "[1] Insert a todo item" << endl
			<< "[2] Connect item" << endl
			<< "[3] Display item related to an item" << endl
			<< "[4] Display all" << endl
			<< "[0] Quit" << endl
			<< "\nEnter Choice: ";
		cin >> choice;
		//cin.ignore();
		while(!cin || cin.peek() != '\n')       // validate the user enters number not a char
		{
			cin.clear();
			cin.ignore(100, '\n');
			cout << "Invalid Number. Try again." << endl
		 	     << "Enter your choice: ";
			cin >> choice;
		}
		cin.get();
		if(choice == 1)	
		{
			cout << "\nEnter todo item to add: ";
			getline(cin, myPlan);
			if(graph.insertVertex(myPlan))
				cout << "\nSuccessfully added" << endl;
			else
				cout << "\nInsert failed" << endl;
		}
		if(choice == 2)
		{
			cout << "\nEnter the first item you want to connect: ";
			getline(cin, myPlan);
			cout << "\nEnter the second item you want to connect to "
				<< myPlan << ": ";
			getline(cin, attachPlan);
			if (graph.insertEdge(myPlan, attachPlan))
				cout << "\nConnection success" << endl;
			else
				cout << "\nConnection failed" << endl;
		}
		if(choice == 3)
		{
			cout << "\nEnter the item you want to display related steps: ";
			getline(cin, myPlan);
			if(!graph.displayAdjacent(myPlan))
				cout << "\nNot Found" << endl;
		}
		if(choice == 4)
		{
			result = graph.displayAll();
			if(result == 0)
				cout << "\nNothing to display" << endl;
		}
	} while (choice != 0);
	
	cout << "\n\nThank you. Have a good day!" << endl << endl;
	return 0;
}
